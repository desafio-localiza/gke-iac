variable "project_id" {
  description = "The project ID to host the cluster in"
  default     = "infra-admin-204520"
}

variable "cluster_name_suffix" {
  description = "A suffix to append to the default cluster name"
  default     = "webinar"
}

variable "region" {
  description = "The region to host the cluster in"
  default     = "us-central1"
}

variable "network" {
  description = "The VPC network to host the cluster in"
  default     = "default-infra-admin"
}

variable "subnetwork" {
  description = "The subnetwork to host the cluster in"
  default     = "default-infra-admin"

}

variable "ip_range_pods" {
  description = "The secondary ip range to use for pods"
  default     = "gke-cluster-pods"
}

variable "ip_range_services" {
  description = "The secondary ip range to use for services"
  default     = "gke-cluster-services"
}

variable "compute_engine_service_account" {
  description = "Service account to associate to the nodes in the cluster"
  default     = "terraform@infra-admin-204520.iam.gserviceaccount.com"
}