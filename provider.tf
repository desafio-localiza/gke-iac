provider "google" {
  #version     = "~> 3.16.0"
  credentials = "credentials/infra-admin-204520-fdc3c8c22f6f.json"
  project     = var.project_id
  region      = "us-central1"
}

provider "google-beta" {
  #version     = "~> 3.16.0"
  credentials = "credentials/infra-admin-204520-fdc3c8c22f6f.json"
  project     = var.project_id
  region      = "us-central1"
}
